# -*- coding:utf-8 -*-
import json
import sqlite3

DB_FILE = "cnlstatus.db"

conn = sqlite3.connect(DB_FILE)
conn.row_factory = sqlite3.Row
c = conn.cursor()

c.execute('update cnlreadout set SensorBGLmg = 1 where ActiveInsulin = -1')

c.execute('select SensorBGLmg, SensorBGLmmol, SensorDate from cnlreadout where strftime(\'%s\',datetime("now","localtime"))-strftime(\'%s\',sensorDate) < 3600 order by sensordate')
rows = c.fetchall()
f = open("chart1h.json", "w")
f.write(json.dumps( [dict(ix) for ix in rows] ))
f.close

c.execute('select SensorBGLmg, SensorBGLmmol, SensorDate from cnlreadout where strftime(\'%s\',datetime("now","localtime"))-strftime(\'%s\',sensorDate) < 3600*3 order by sensordate')
rows = c.fetchall()
f = open("chart3h.json", "w")
f.write(json.dumps( [dict(ix) for ix in rows] ))
f.close

c.execute('select SensorBGLmg, SensorBGLmmol, SensorDate from cnlreadout where strftime(\'%s\',datetime("now","localtime"))-strftime(\'%s\',sensorDate) < 3600*6 order by sensordate')
rows = c.fetchall()
f = open("chart6h.json", "w")
f.write(json.dumps( [dict(ix) for ix in rows] ))
f.close

c.execute('select SensorBGLmg, SensorBGLmmol, SensorDate from cnlreadout where strftime(\'%s\',datetime("now","localtime"))-strftime(\'%s\',sensorDate) < 3600*12 order by sensordate')
rows = c.fetchall()
f = open("chart12h.json", "w")
f.write(json.dumps( [dict(ix) for ix in rows] ))
f.close

c.execute('select SensorBGLmg, SensorBGLmmol, SensorDate from cnlreadout where strftime(\'%s\',datetime("now","localtime"))-strftime(\'%s\',sensorDate) < 360024 order by sensordate')
rows = c.fetchall()
f = open("chart24h.json", "w")
f.write(json.dumps( [dict(ix) for ix in rows] ))
f.close

conn.commit()
c.close()
