# -*- coding:utf-8 -*-
import json
import sqlite3

JSON_FILE = "cnl.json"
DB_FILE = "cnlstatus.db"

jsondata = json.load(open(JSON_FILE))
conn = sqlite3.connect(DB_FILE)

ActiveInsulin = jsondata["status"]["ActiveInsulin"]
SensorBGLmg = jsondata["status"]["SensorBGLmg"]
SensorBGLmmol = jsondata["status"]["SensorBGLmmol"]
SensorDate = jsondata["status"]["SensorDate"]
BGLtrend = jsondata["status"]["BGLtrend"]
Currentbasalrate = jsondata["status"]["Currentbasalrate"]
Tempbasalrate = jsondata["status"]["Tempbasalrate"]
Tempbasalpercentage = jsondata["status"]["Tempbasalpercentage"]
Unitsremaining = jsondata["status"]["Unitsremaining"]
Batteryremaining = jsondata["status"]["Batteryremaining"]
PumpStart = jsondata["status"]["Pumphistoryinfo"]["PumpStart"]
PumpEnd = jsondata["status"]["Pumphistoryinfo"]["PumpEnd"]
PumpSize = jsondata["status"]["Pumphistoryinfo"]["PumpSize"]

data = (ActiveInsulin, SensorBGLmg, SensorBGLmmol, BGLtrend, Currentbasalrate, Tempbasalrate, Tempbasalpercentage, Unitsremaining, Batteryremaining, PumpStart, PumpEnd, PumpSize)

c = conn.cursor()
c.execute('create table if not exists cnlreadout (ActiveInsulin text, SensorBGLmg text, SensorBGLmmol text, SensorDate datetime, BGLtrend text, Currentbasalrate text, Tempbasalrate text, Tempbasalpercentage text, Unitsremaining text, Batteryremaining text, PumpStart text, PumpEnd text, PumpSize text)')
c.execute('insert into cnlreadout values (?,?,?,datetime("now", "localtime"),?,?,?,?,?,?,?,?,?)', data)

conn.commit()
c.close()
