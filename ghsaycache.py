#! /usr/bin/python3.5

# Mostly based on code listed on
# https://www.gioexperience.com/google-home-hack-send-voice-programmaticaly-with-python/
# Usage:
# python3 ghsaycache.py "Alarm Text" en
# Provide text for notification as first parameter. Use double quotes where needed
# Provide language for notification text as second parameter. Default is "en" for English

# This script is used in bash script to pregenerate the audio messages from 40 to 300 through Google TTS API
# Run through ghsaycache.sh (or local variety ghsaycache.nl.sh)

# Uses Python 3
# Install dependencies:
# pip3 install gtts

import sys
import os
import os.path
from gtts import gTTS
import time
import hashlib

say=sys.argv[1];
language="en";

#********* retrieve local ip of my rpi3
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
local_ip=s.getsockname()[0]
s.close()
#**********************

try:
   language=sys.argv[2];
except:
  pass

fname=hashlib.md5(say.encode()).hexdigest()+".mp3"; #create md5 filename for caching

try:
   os.mkdir("mp3_cache")
except:
   pass

if not os.path.isfile("mp3_cache/"+fname):
   tts = gTTS(say,lang=language) # Change language here
   tts.save("mp3_cache/"+fname)
